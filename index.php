
<?php
// Header template insert
require('includes/header.php');


// Get all data


if(isset($_GET['sort'])){
    switch ($_GET['sort']) {
        case "plaatsingsdatum":
            $statement = $db->prepare("select * from apps where app_prive = 0  ORDER BY app_plaatsingsdatum");
            break;
        case "updatedatum":
            $statement = $db->prepare("select * from apps where app_prive = 0  ORDER BY app_updatedatum");
            break;
        case "naam":
            $statement = $db->prepare("select * from apps where app_prive = 0  ORDER BY app_naam");
            break;
        case "giveup":
            header( "Location: https://www.youtube.com/watch?v=dQw4w9WgXcQ" );
            break;
        default:
            $statement = $db->prepare("select * from apps where app_prive = 0  ");
            break;
    }
}else {
    $statement = $db->prepare("select * from apps where app_prive = 0 ");
}

$statement->execute();
$rows = $statement->fetchAll();
?>

<?php
if(isset($_GET['filter'])){
    switch ($_GET['filter']) {
        case "educatief":
            $statement = $db->prepare("select * from apps where app_prive = 0 AND app_type='Educatief en onderwijs'");
            break;
        case "entertainment":
            $statement = $db->prepare("select * from apps where app_prive = 0 AND app_type='Entertainment'");
            break;
        case "evenementen":
            $statement = $db->prepare("select * from apps where app_prive = 0 AND app_type='Evenementen'");
            break;
        case "lifestyle":
            $statement = $db->prepare("select * from apps where app_prive = 0 AND app_type='Lifestyle'");
            break;
        case "nieuws":
            $statement = $db->prepare("select * from apps where app_prive = 0 AND app_type='Nieuws en tijdschriften'");
            break;
        case "productiviteit":
            $statement = $db->prepare("select * from apps where app_prive = 0 AND app_type='Productiviteit'");
            break;
        case "reizen":
            $statement = $db->prepare("select * from apps where app_prive = 0 AND app_type='Reizen en lokaal'");
            break;
        case "sport":
            $statement = $db->prepare("select * from apps where app_prive = 0 AND app_type='Sport'");
            break;
        case "zakelijk":
            $statement = $db->prepare("select * from apps where app_prive = 0 AND app_type='Zakelijk'");
            break;
        default:
            $statement = $db->prepare("select * from apps where app_prive = 0  ");
            break;
    }
    $statement->execute();
    $rows = $statement->fetchAll();
}else {
    $statement = $db->prepare("select * from apps where app_prive = 0 ");
}

?>
<!-- Page Content -->
<div class="app">
    <div class="container">

        <h1 class="my-4 text-center text-lg-left">Onze Apps</h1>

        <p>
            Sorteer:
            <select name="select-style" onchange="location = this.value;">
                <option <?php if(isset($_GET['sort']) &&  $_GET['sort'] == "") { echo "selected ";}  ?> value="index.php?sort=">Maak Je Keuze..</option>
                <option <?php if(isset($_GET['sort']) &&  $_GET['sort'] == "plaatsingsdatum") { echo "selected ";}  ?> value="index.php?sort=plaatsingsdatum">Plaatsings datum</option>
                <option <?php if(isset($_GET['sort']) &&  $_GET['sort'] == "updatedatum") { echo "selected ";}  ?> value="index.php?sort=updatedatum">Update datum</option>
                <option <?php if(isset($_GET['sort']) &&  $_GET['sort'] == "naam") { echo "selected ";}  ?> value="index.php?sort=naam">Naam</option>
            </select>
            Filter:
            <select name="select-style" onchange="location = this.value;">
                <option <?php if(isset($_GET['filter']) &&  $_GET['filter'] == "") { echo "selected ";}  ?> value="index.php?filter=">Maak Je Keuze..</option>
                <option <?php if(isset($_GET['filter']) &&  $_GET['filter'] == "educatief") { echo "selected ";}  ?> value="index.php?filter=educatief">Educatief en onderwijs</option>
                <option <?php if(isset($_GET['filter']) &&  $_GET['filter'] == "entertainment") { echo "selected ";}  ?> value="index.php?filter=entertainment">Entertainment</option>
                <option <?php if(isset($_GET['filter']) &&  $_GET['filter'] == "evenementen") { echo "selected ";}  ?> value="index.php?filter=evenementen">Evenementen</option>
                <option <?php if(isset($_GET['filter']) &&  $_GET['filter'] == "nieuws") { echo "selected ";}  ?> value="index.php?filter=nieuws">Nieuws en tijdschriften</option>
                <option <?php if(isset($_GET['filter']) &&  $_GET['filter'] == "productiviteit") { echo "selected ";}  ?> value="index.php?filter=productiviteit">Productiviteit</option>
                <option <?php if(isset($_GET['filter']) &&  $_GET['filter'] == "reizen") { echo "selected ";}  ?> value="index.php?filter=reizen">Reizen en lokaal</option>
                <option <?php if(isset($_GET['filter']) &&  $_GET['filter'] == "sport") { echo "selected ";}  ?> value="index.php?filter=sport">Sport</option>
                <option <?php if(isset($_GET['filter']) &&  $_GET['filter'] == "zakelijk") { echo "selected ";}  ?> value="index.php?filter=zakelijk">Zakelijk</option>
            </select>
        </p>

        <div class="row text-center text-lg-left">
            <?php
            foreach($rows as $row)
            { ?>
                <div class="col-lg-3 col-md-4 col-xs-6">
                    <a href="" data-toggle="modal" data-target=".modalApp<?php echo $row['app_id'] ?>"  class="d-block mb-4 h-100">
                        <img class="img-fluid img-thumbnail" src="<?php echo $row['app_foto'] ?>" alt="<?php echo $row['app_id'] ?>">
                        <p><?php echo $row['app_naam'] ?></p>
                    </a>
                </div>
           <?php }
            ?>


        </div>

    </div>
<!-- /.container -->
</div>

<?php
foreach($rows as $row)
{ ?>

    <!-- Modal -->
    <div class="modal fade modalApp<?php echo $row['app_id'] ?>" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLongTitle"><?php echo $row['app_naam'] ?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-weight: bold;" for="naam" class="col-form-label">Naam: </label>
                                <li id="naam" class="list-group"><?php echo $row['app_naam'] ?></li>
                                <label style="font-weight: bold;" for="type" class="col-form-label">Type: </label>
                                <li id="type" class="list-group"><?php echo $row['app_type'] ?></li>
                                <label style="font-weight: bold;" for="klant" class="col-form-label">Eigenaar: </label>
                                <li id="" class="list-group"><?php echo $row['app_klant'] ?></li>
                                <label style="font-weight: bold;" for="klant" class="col-form-label">Link: </label>
                                <li id="" class="list-group"> <a target="_blank" style="word-wrap: break-word; white-space: normal;" href=" <?php echo $row['app_link'] ?>"> Android  </a></li>
                                <?php  if($row['app_linkIOS'] != null) { ?>
                                <li id="" class="list-group"> <a target="_blank" style="word-wrap: break-word; white-space: normal;" href=" <?php echo $row['app_linkIOS'] ?>"> iPhone  </a></li>
                                <?php } ?>
                            </div>
                            <div class="col-md-4">
                                <label style="font-weight: bold;" for="oms" class="col-form-label">Omschrijving: </label>
                                <li id="oms" class="list-group"><?php echo $row['app_oms'] ?></li>
                            </div>
                            <div class="col-md-4 ml-auto">
                                <img class="img-fluid img-thumbnail-sm" src="<?php echo $row['app_foto'] ?>" alt="<?php echo $row['app_id'] ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="container-fluid">
                        <div class="row">
                            <?php
                            $teller = 0;
                            foreach(glob('./images/'.   $row["app_id"]. '/SH/*.*') as $filename){ ?>
                            <a class="lightbox" href="#foto<?php echo $row['app_id'] . $teller; ?>">
                                <div style="position: inherit;" class="col-md-4">

                                    <img src="<?php echo $filename ?>">

                                </div>
                            </a>
                                <!-- De lightbox hemzelf, -->
                                <div class="lightbox-target" id="foto<?php echo $row['app_id'] . $teller;  ?>">
                                    <img src="<?php echo $filename ?>"/>
                                    <a class="lightbox-close" href="#"></a>
                                </div>

                        <?php $teller++; } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }
?>


<?php
// Header template insert
require('includes/footer.php');

?>

<script>

</script>

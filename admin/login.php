<?php

require('../includes/header.php');
require_once('classes/user.php');

$user = new User($db);


if( $user->is_logged_in() ){ header('Location: ../index.php'); exit(); }


if(isset($_POST['submit'])){

	if (!isset($_POST['username'])) $error[] = "Vull In Alle Velden";
	if (!isset($_POST['password'])) $error[] = "Vull In Alle Velden";

	$username = $_POST['username'];
	if ( $user->isValidUsername($username)){
		if (!isset($_POST['password'])){
			$error[] = 'Wachtwoord mag niet leeg zijn';
		}
		$password = $_POST['password'];

		if($user->login($username,$password)){
			$_SESSION['username'] = $username;
			header('Location: admin.php');
			exit;

		} else {
			$error[] = 'Gebruikersnaam en/of wachtwoord komt niet overeen.';
		}
	}else{
		$error[] = 'Onbekende error';
	}



}



?>

	
<div class="container">

	<div class="row">

	    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form" method="post" action="" autocomplete="off">
				<h2>Admin login</h2>
				<p><a href='../index.php'>Terug naar homepage</a></p>
				<hr>

				<?php
				//check for any errors
				if(isset($error)){
					foreach($error as $error){
						echo '<p  style="color: white;" class="bg-danger">'.$error.'</p>';
					}
				}
				?>

				<div class="form-group">
					<input type="text" name="username" id="username" class="form-control input-lg" placeholder="Gebruikersnaam" value="<?php if(isset($error)){ echo htmlspecialchars($_POST['username'], ENT_QUOTES); } ?>" tabindex="1">
				</div>

				<div class="form-group">
					<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Wachtwoord" tabindex="3">
				</div>

				
				<hr>
				<div class="row">
					<div class="col-xs-6 col-md-6"><input type="submit" name="submit" value="Login" class="btn btn-primary btn-block btn-lg" tabindex="5"></div>
				</div>
			</form>
		</div>
	</div>



</div>


<?php 
//include header template
require('../includes/footer.php');
?>

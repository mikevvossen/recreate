<?php
require('../includes/header.php');
require_once('classes/user.php');

$user = new User($db);

$statement = $db->prepare("SELECT app_id FROM apps ORDER BY app_id DESC LIMIT 1");
$statement->execute();
$LastInsertedId = $statement->fetch();

$statement = $db->prepare("SELECT `AUTO_INCREMENT`
FROM  INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'recreate'
AND   TABLE_NAME   = 'apps';");
$statement->execute();
$LastInsertedId = $statement->fetch();

$id = $LastInsertedId[0];



if(isset($_POST['aanpassen'])){
    
    // LOGO HANDELAAR
    if(isset($_FILES["logo"]) && $_FILES["logo"] != null){

        $files = glob("../images/".$_POST['app_id']."/Logo.png");
        foreach($files as $file){
            if(is_file($file))
                unlink($file);
        }

        $target_dir_logo = "../images/".$_POST['app_id']."/";

        $target_file = $target_dir_logo . basename($_FILES["logo"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        $RenameLogo = 'Logo.' . $imageFileType;

        $target_file = $target_dir_logo . $RenameLogo;

        if($imageFileType == "jpg"  && $imageFileType == "jpeg"
            && $imageFileType = "gif" ) {
            $uploadOk = 1;
            ?>
            <div class="container">
                <br><br><br>
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>wij accepteren alleen png!</strong>
                </div>
            </div>
            <?php
        }

        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        } else {
            move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file);
        }
    }

    if(isset($_POST['app_prive']) && $_POST['app_prive'] == 'prive'){
        $prive = (int)1;
    }else{
        $prive = (int)0;
    }


    $sql = "UPDATE apps SET 
            app_naam = :naam, 
            app_type = :apptype, 
            app_klant = :klant,  
            app_link = :link,  
            app_linkIOS = :linkIOS,  
            app_updatedatum = :updatedatum,
            app_oms = :oms,
            app_prive = :prive
            WHERE app_id = :appid";

    $naam = htmlentities($_POST['app_naam']);
    $omschrijving = htmlentities($_POST['oms']);

    $stmt = $db->prepare($sql);
    $stmt->bindParam(':naam', $naam, PDO::PARAM_STR);
    $stmt->bindParam(':apptype', $_POST['app_type'], PDO::PARAM_STR);
    $stmt->bindParam(':klant', $_POST['app_klant'], PDO::PARAM_STR);
    $stmt->bindParam(':link', $_POST['app_link'], PDO::PARAM_STR);
    $stmt->bindParam(':linkIOS', $_POST['app_linkIOS'], PDO::PARAM_STR);
    $stmt->bindParam(':updatedatum', $_POST['app_updatedatum'], PDO::PARAM_STR);
    $stmt->bindParam(':oms', $omschrijving, PDO::PARAM_STR);
    $stmt->bindParam(':prive', $prive, PDO::PARAM_INT);
    $stmt->bindParam(':appid', $_POST['app_id'], PDO::PARAM_INT);

    if($stmt->execute()){ ?>
        <div class="container">
            <br><br><br>
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Gelukt!</strong> Record Gewijzigd.
            </div>
        </div>
    <?php }else{ ?>
        <div class="container">
            <br><br><br>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Helaas!</strong> Er ging iets mis met updaten.
            </div>
        </div>
    <?php }
}

if(isset($_POST['delete'])){
    $dir = "../images/".$_POST['app_id'];
    $dirSH = "../images/".$_POST['app_id']."/";



    $filesSH = glob("../images/".$_POST['app_id']."/SH/*");
    foreach($filesSH as $files){
        if(is_file($files))
            unlink($files);
    }

    $files = glob("../images/".$_POST['app_id']."/*");
    foreach($files as $file){
        if(is_file($file))
            unlink($file);
        if(is_dir($file)){
            rmdir($file);
        }
    }

    rmdir($dir);

    $sql = "DELETE FROM apps WHERE app_id= :id";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $_POST['app_id'], PDO::PARAM_INT);

    if($stmt->execute()){ ?>
        <div class="container">
            <br><br><br>
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Gelukt!</strong> Record verwijderd.
            </div>
        </div>
    <?php }else{ ?>
        <div class="container">
            <br><br><br>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Helaas!</strong> Er ging iets mis met verwijderen.
            </div>
        </div>
    <?php }

}


if(isset($_POST['toevoegen'])){





    if (!file_exists("images/".$id)) {
        mkdir("../images/".$id, 0777, true);
    }

    if (!file_exists("images/".$id."/SH")) {
        mkdir("../images/".$id. "/SH", 0777, true);
    }

    //SCREENSHOT HANDELAAR
     if(isset($_FILES["screenshot"]) && $_FILES["screenshot"] != null){

         if(count($_FILES['screenshot']['name']) > 0) {

             for ($i = 0; $i < count($_FILES['screenshot']['name']); $i++) {

                 $tmpFilePath = $_FILES['screenshot']['tmp_name'][$i];


                 if ($tmpFilePath != "") {


                     $shortname = $_FILES['screenshot']['name'][$i];


                     $filePath = "../images/".$id."/SH/" . date('d-m-Y-H-i-s').'-'.$_FILES['screenshot']['name'][$i];



                    move_uploaded_file($tmpFilePath, $filePath);

                 }
             }
         }


    }

    // LOGO HANDELAAR
    if(isset($_FILES["logo"]) && $_FILES["logo"] != null){

    $target_dir_logo = "../images/".$id."/";

    $target_file = $target_dir_logo . basename($_FILES["logo"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

    $RenameLogo = 'Logo.' . $imageFileType;

    $target_file = $target_dir_logo . $RenameLogo;

    if($imageFileType == "jpg"  && $imageFileType == "jpeg"
        && $imageFileType = "gif" ) {
        $uploadOk = 1;
        ?>
        <div class="container">
            <br><br><br>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>wij accepteren alleen png!</strong>
            </div>
        </div>
        <?php
    }

    if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
    } else {
        move_uploaded_file($_FILES["logo"]["tmp_name"], $target_file);
    }
    }
        if (isset($_POST['app_prive']) && $_POST['app_prive'] == 'prive') {
            $prive = (int)1;
        } else {
            $prive = (int)0;
        }


    $sql = "INSERT INTO apps SET 
            app_naam = :naam, 
            app_type = :apptype, 
            app_klant = :klant,  
            app_link = :link,  
            app_linkIOS = :linkIOS,  
            app_updatedatum = :updatedatum,
            app_plaatsingsdatum = :plaatsingsdatum,
            app_oms = :oms,
            app_prive = :prive,
            app_foto = :foto";

    $naam = htmlentities($_POST['app_naam']);
    $omschrijving = htmlentities($_POST['oms']);
    $type = htmlentities($_POST['app_type']);
    $klant = htmlentities($_POST['app_klant']);
    $link = htmlentities($_POST['app_link']);
    $linkIOS = htmlentities($_POST['app_linkIOS']);
    $updatedatum = htmlentities($_POST['app_updatedatum']);
    $plaatsingsdatum = htmlentities($_POST['app_plaatsingsdatum']);


    $stmt = $db->prepare($sql);
    $stmt->bindParam(':naam', $naam, PDO::PARAM_STR);
    $stmt->bindParam(':apptype', $type, PDO::PARAM_STR);
    $stmt->bindParam(':klant', $klant, PDO::PARAM_STR);
    $stmt->bindParam(':link',$link, PDO::PARAM_STR);
    $stmt->bindParam(':linkIOS',$linkIOS, PDO::PARAM_STR);
    $stmt->bindParam(':updatedatum', $updatedatum, PDO::PARAM_STR);
    $stmt->bindParam(':plaatsingsdatum', $plaatsingsdatum, PDO::PARAM_STR);
    $stmt->bindParam(':oms', $omschrijving, PDO::PARAM_STR);
    $stmt->bindParam(':prive', $prive, PDO::PARAM_INT);
    $stmt->bindParam(':foto', $target_file, PDO::PARAM_STR);

    if($stmt->execute()){ ?>
        <div class="container">
            <br><br><br>
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Gelukt!</strong> Record toegevoegd.
            </div>
        </div>
    <?php }else{ ?>
        <div class="container">
            <br><br><br>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Helaas!</strong> Er ging iets mis met toevoegen.
            </div>
        </div>
    <?php }
}

if(!$user->is_logged_in()){ header('Location: login.php'); exit(); }
// Get all data
$statement = $db->prepare("select * from apps ");
$statement->execute();
$rows = $statement->fetchAll();
?>
<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <h1 class="my-4 text-center text-lg-left">Beheerpaneel</h1>
        </div>
    </div>
    <div class="app">
        <div class="row text-center text-lg-left">
            <?php
            foreach($rows as $row)
            { ?>
                <div class="col-lg-3 col-md-4 col-xs-6">
                    <a href="" data-toggle="modal" data-target=".modalApp<?php echo $row['app_id'] ?>"  class="d-block mb-4 h-100">
                        <img class="img-fluid img-thumbnail" src="<?php echo $row['app_foto'] ?>" alt="<?php echo $row['app_id'] ?>">
                        <p><?php echo $row['app_naam'] ?></p>
                    </a>
                </div>
            <?php }
            ?>
            <div class="col-lg-3 col-md-4 col-xs-6">
                <a href="" data-toggle="modal" data-target=".modalAppAdd" class="d-block mb-4 h-100">
                    <img class="img-fluid img-thumbnail" src="/images/addsign.png">
                    Voeg een app toe
                </a>
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>

<?php
foreach($rows as $row){?>

    <!-- Modal Edit -->
    <div class="modal fade modalApp<?php echo $row['app_id'] ?>" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLongTitle"><?php echo $row['app_naam'] ?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="container-fluid">
                        <form method="post" enctype="multipart/form-data">
                            <div class=" form-group row">
                                <div class="col-md-4">
                                    <input type="hidden" id="id" name="app_id" class="form-control" value="<?php echo $row['app_id'] ?>">
                                    <label style="font-weight: bold;" for="naam" class="col-form-label">Naam: </label>
                                    <input type="text" id="naam" name="app_naam" class="form-control" value="<?php echo $row['app_naam'] ?>">
                                    <label style="font-weight: bold;" for="type" class="col-form-label">Type: </label>
                                    <input type="text" id="type" name="app_type" class="form-control" value="<?php echo $row['app_type'] ?>">
                                    <label style="font-weight: bold;" for="klant" class="col-form-label">Eigenaar: </label>
                                    <input type="text" id="klant" name="app_klant" class="form-control" value="<?php echo $row['app_klant'] ?>">
                                    <label style="font-weight: bold;" for="link" class="col-form-label">Android Link: </label>
                                    <input type="text" id="link" name="app_link" class="form-control" value="<?php echo $row['app_link'] ?>">
                                    <label style="font-weight: bold;" for="link" class="col-form-label">IOS Link: </label>
                                    <input type="text" id="link" name="app_linkIOS" class="form-control" value="<?php echo $row['app_linkIOS'] ?>">
                                    <label style="font-weight: bold;" for="update" class="col-form-label">UpdateDatum: </label>
                                    <input type="date" id="update" name="app_updatedatum" class="form-control" value="<?php echo $row['app_updatedatum'] ?>">
                                    <label style="font-weight: bold;" for="app_prive" class="col-form-label">Prive App</label>
                                    <input type="checkbox" <?php if($row['app_prive'] == 1) { echo "checked"; } ?> name="app_prive" id="app_prive" value="prive"><br>
                                    <label style="font-weight: bold;" for="logo" class="col-form-label">Logo ( PNG ): </label>
                                    <input type="file" name="logo" id="logo">
                                </div>
                                <div class="col-md-4">
                                    <label style="font-weight: bold;" for="oms" class="col-form-label">Omschrijving: </label>
                                    <textarea id="oms" rows="13" name="oms" class="form-control"><?php echo $row['app_oms'] ?></textarea>
                                </div>
                                <div class="col-md-4 ml-auto">
                                    <img class="img-fluid img-thumbnail-sm" src="<?php echo $row['app_foto'] ?>" alt="<?php echo $row['app_id'] ?>">
                                    <br><br>
                                </div>
                            </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="delete" class="btn-danger btn-lg">Verwijderen</button>
                    <button type="submit" name="aanpassen" class="btn btn-lg">Aanpassen</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php }
?>

    <!-- Modal Add -->
    <div class="modal fade modalAppAdd" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLongTitle">Voeg app toe</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="container-fluid">
                        <form method="post" enctype="multipart/form-data">
                            <div class=" form-group row">
                                <div class="col-md-4">
                                    <input type="hidden" id="id" name="app_id" class="form-control" value="">
                                    <label style="font-weight: bold;" for="naam" class="col-form-label">Naam: </label>
                                    <input type="text" id="naam" name="app_naam" class="form-control" value="">
                                    <label style="font-weight: bold;" for="type" class="col-form-label">Type: </label>
                                    <input type="text" id="type" name="app_type" class="form-control" value="">
                                    <label style="font-weight: bold;" for="klant" class="col-form-label">Eigenaar: </label>
                                    <input type="text" id="klant" name="app_klant" class="form-control" value="">
                                    <label style="font-weight: bold;" for="link" class="col-form-label">Android Link: </label>
                                    <input type="text" id="link" name="app_link" class="form-control" value="">
                                    <label style="font-weight: bold;" for="link" class="col-form-label">IOS Link: </label>
                                    <input type="text" id="link" name="app_linkIOS" class="form-control" value="">
                                    <label style="font-weight: bold;" for="update" class="col-form-label">UpdateDatum: </label>
                                    <input type="date" id="update" name="app_updatedatum" class="form-control" value="<?php echo date("Y-m-d"); ?>">
                                    <label style="font-weight: bold;" for="plaatsing" class="col-form-label">PlaatsingsDatum: </label>
                                    <input type="date" id="plaatsing" name="app_plaatsingsdatum" class="form-control" value="<?php echo date("Y-m-d"); ?>">
                                    <label style="font-weight: bold;" for="app_prive" class="col-form-label">Prive App: </label>
                                    <input type="checkbox"  name="app_prive" id="app_prive" value="prive"><br>
                                    <label style="font-weight: bold;" for="logo" class="col-form-label">Logo ( PNG ): </label>
                                    <input type="file" name="logo" id="logo">
                                    <label style="font-weight: bold;" for="screenshot" class="col-form-label">Screenshots ( Meerdere afb. ) ( PNG ): </label>
                                    <input type="file" multiple name="screenshot[]" id="screenshot">
                                </div>
                                <div class="col-md-4">
                                    <label style="font-weight: bold;" for="oms" class="col-form-label">Omschrijving: </label>
                                    <textarea id="oms" rows="13" name="oms" class="form-control"></textarea>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="toevoegen" class="btn btn-lg">Toevoegen</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<?php
//include header template
require('../includes/footer.php');
?>

<?php
include("config.php")

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link async rel="shortcut icon" href="../images/favicon.ico">
    <link async rel="apple-touch-icon" href="../images/apple-touch-icon.png">
    <link async rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
    <link async rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">

    <title><?php echo $site_title; ?></title>


    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="../css/thumbnail-gallery.css" rel="stylesheet">
    <link href="../css/stylesheet.css" rel="stylesheet">



</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/index.php"><img src="../images/recreate_white.png" class="recreate_logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <?php
                    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){ ?>
                        <a class="nav-link" href="/admin/admin.php">Beheerpaneel</a>
                    <?php }else{ ?>

                    <?php }
                    ?>

                </li>
                <li class="nav-item">
                    <?php
                    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){ ?>
                        <a class="nav-link" href="/admin/logout.php">Logout</a>
                    <?php }else{ ?>
                        <a class="nav-link" href="/admin/login.php">Login</a>
                   <?php }
                    ?>

                </li>
            </ul>
        </div>

    </div>
</nav>
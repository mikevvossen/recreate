<?php include("includes/header.php"); ?>
<div class="app">
    <div class="container">

        <h1 class="my-4 text-center text-lg-left">Credits</h1>
        <h5 class="my-1 text-center text-lg-left">Dit project is gemaakt in opdracht van Recreate in naam van ROC van Twente.<br>
        Wij willen Recreate en ROC bedanken voor een leerzame week. <br>
        Deze website is gemaakt door:</h5>

        <ul class="list-group">
            <li class="list-group-item">Maarten </li>
            <li class="list-group-item">Francy </li>
            <li class="list-group-item">Lennart</li>
            <li class="list-group-item">Mike</li>
        </ul>



    </div>
    <!-- /.container -->
</div>
<?php include("includes/footer.php"); ?>

